//
//  ViewController.swift
//  AmzingProduct
//
//  Created by david florczak on 05/12/2019.
//  Copyright © 2019 david florczak. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet weak var myLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        myLabel.text = "David is the best"
        myLabel.textColor = .red
        myLabel.backgroundColor = .green
    }


}

